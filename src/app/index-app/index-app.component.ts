import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-index-app',
  templateUrl: './index-app.component.html',
  styleUrls: ['./index-app.component.css']
})
export class IndexAppComponent implements OnInit {
  cars: any;

  constructor(private service: AppService) { }

  ngOnInit() {
    this.getCars();
  }
  
  getCars() {
    this.service.getCars().subscribe(
      response => this.cars = response,
      error => console.error(error),
    );
  }

  onDelete(carId, carName) {
    const confirmation = confirm('Apakah anda yakin ingin menghapus ' + carName);
    if (confirmation) {
      this.service.deleteCar(carId).subscribe(
        response => {
          alert(`${carName} berhasil dihapus`)
          this.getCars();
        },
        error => alert(`${carName} gagal dihapus`),
      );
    }
  }
}
