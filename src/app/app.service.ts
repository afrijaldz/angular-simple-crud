import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AppService {
  constructor(private httpClient: HttpClient) { }

  getCars() {
    return this.httpClient.get('http://localhost:3000/cars');
  }

  postCar(data) {
    return this.httpClient.post('http://localhost:3000/cars', data);
  }

  getCarById(id) {
    return this.httpClient.get(`http://localhost:3000/cars/${id}`);
  }

  updateCar(id, data) {
    return this.httpClient.put(`http://localhost:3000/cars/${id}`, data);
  }

  deleteCar(id) {
    return this.httpClient.delete(`http://localhost:3000/cars/${id}`);
  }
  
}