import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-app',
  templateUrl: './edit-app.component.html',
  styleUrls: ['./edit-app.component.css']
})
export class EditAppComponent implements OnInit {
  form: FormGroup;
  car: any;

  constructor(private router: Router, private service: AppService, private route: ActivatedRoute) { }
  
  ngOnInit() {
    this.car = this.route.snapshot.data['data'];
    this.buildForm();
  }

  buildForm() {
    this.form = new FormGroup({
      'nama': new FormControl(this.car.nama),
      'tahun': new FormControl(this.car.tahun),
    });
  }

  onSubmit() {
    const id = this.car.id;
    const data = this.form.value;

    this.service.updateCar(id, data).subscribe(
      response => this.router.navigate(['/']),
      error => alert('error' + error),
    );
  }

}
