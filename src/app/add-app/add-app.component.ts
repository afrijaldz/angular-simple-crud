import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-app',
  templateUrl: './add-app.component.html',
  styleUrls: ['./add-app.component.css']
})
export class AddAppComponent implements OnInit {
  form: FormGroup;

  constructor(private service: AppService, private router: Router) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = new FormGroup({
      'nama': new FormControl(null, [Validators.required]),
      'tahun': new FormControl(null, [Validators.required]),
    });
  }

  onSubmit() {
    const data = this.form.value;
    this.service.postCar(data).subscribe(
      response => {
        this.router.navigate(['/']);
      },
      error => {
        alert('error' + error);
      }
    );
  }

}
