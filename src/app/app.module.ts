import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { CarResolver } from './resolver/car.resolver';

import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { HttpClientModule } from '@angular/common/http';
import { EditAppComponent } from './edit-app/edit-app.component';
import { AddAppComponent } from './add-app/add-app.component';
import { IndexAppComponent } from './index-app/index-app.component';

const routes: Routes = [
  { path: '', component: IndexAppComponent },
  { path: 'add', component: AddAppComponent },
  { path: 'edit/:id', component: EditAppComponent, resolve: { data: CarResolver } },
];


@NgModule({
  declarations: [
    AppComponent,
    EditAppComponent,
    AddAppComponent,
    IndexAppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
  ],
  providers: [AppService, CarResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
